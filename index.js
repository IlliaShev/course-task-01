const express = require('express');
const dayjs = require('dayjs');
const isLeapYear = require('dayjs/plugin/isLeapYear');
dayjs.extend(isLeapYear);
const app = express();
const PORT = process.env.PORT ?? 56201;

const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

app.use(express.text(['text/plain']));

app.post('/square', (req, res) => {
    const number = +req.body;
    const square = number * number;
    res.send({
        'number': number,
        'square': square
    });
});

app.post('/reverse', (req, res) => {
    const text = req.body;
    const reversedString = text.split('').reverse().join('');
    res.send(reversedString);
});

app.get('/date/:year/:month/:day', (req, res) => {
    const year = req.params.year;
    const month = --req.params.month;
    const day = req.params.day;   
    const date = dayjs(new Date(year, month, day));
    const now = dayjs();
    const dateToDiff = dayjs(new Date(now.year(), now.month(), now.day()));
    res.send({
        'weekDay': daysOfWeek[date.day()],
        'isLeapYear': date.isLeapYear(),
        'difference': Math.abs(Number(date.diff(dateToDiff, 'day')) + 1) 
    })
});

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
})